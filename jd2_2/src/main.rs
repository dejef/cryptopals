extern crate base64;
extern crate openssl;
extern crate crypto_tools;

use openssl::symm::{encrypt, Cipher};
use crypto_tools::util::*;

static FILENAME: &'static str = "lyrics.txt";
static KEY: &'static [u8] = b"YELLOW SUBMARINE";
const BLOCK_SIZE: usize = 16;

// Rolling our own CBC with ECB in a loop
fn main() -> std::io::Result<()> {

    // grab text from the file lyrics.txt
    let mut text: Vec<u8> = vec_char_to_u8(file_to_vec(FILENAME.to_string()));

    // use ECB cipher mode and XOR to implement CBC
    let cipher = Cipher::aes_128_ecb();

    // count up to block size then decrypt that block 
    let mut count = 0;

    // vector to be written to file, to contain all encrypted text 
    let mut soln: Vec<u8> = vec![];
    
    // the previously encrypted block to be XOR'd 
    let mut prev: Vec<u8> = vec![0;16];

    // blocks must be 32 bytes long, contains characters extracted from lyrics.txt 
    let mut block: Vec<u8> = vec![];

    // the text after decoded needs to be length % 16 == 0 so pad it
    pad(BLOCK_SIZE, &mut text).expect("shtf");

    // this is a horrible hack to get another iteration out of the below for loop
    text.push(0_u8);

    // breaks up the text into block-sized (32 bytes) chunks
    for chr in text {

        // for every chunk begin the process of encryption
        if count % BLOCK_SIZE == 0 && count != 0 {

            // need this guy for switching over from curr to prev
            let mut temp = vec![];
            
            // The XOR operation that should be its own function
            for elem in &block {
                temp.push(elem ^ prev.remove(0));
            }

            // The OpenSSL call that should definitely be its own call
            let mut curr = encrypt(cipher, KEY, None, &temp).expect("encrypt failed");
            
            // curr is 32 bytes in length. Whack off the first 16 bytes and keep em
            curr.split_off(BLOCK_SIZE);

            // need to store in temp because this curr will die when appended to soln
            temp = curr.clone();

            // attach the encrypted cipher text to the stream of others (soln)
            soln.append(&mut curr); 

            // feed the curr encypted block into the iteration so we're ~chaining~
            prev = temp;
            
            // reset the block for the next iteration
            block.clear();

            //reset count
            count = 0;
        }

        // push the character onto this block
        block.push(chr); 
        
        // increment count to catch the end of the block
        count += 1;
    }

    Ok(())
}

// Decode the text    
//
//    let mut data = base64::decode_config(&text, base64::MIME).expect("base64 decode data");
//    let cipher = Cipher::aes_128_cbc();
//    let new_data = decrypt(cipher, KEY, Some(&[0;16]), &data);
//    let new_data = String::from_utf8(new_data.unwrap()).expect("UTF8 decode failed");
//    println!("{:?}", new_data);          
//    let mut file = File::create("foo.txt")?;
//    file.write_all(base64::encode(&soln).as_bytes())?;
//    return Ok(());
