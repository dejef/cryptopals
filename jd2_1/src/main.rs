static KEY: &'static [u8] = b"YELLOW SUBMARINE";

fn main() -> Result<(), &'static str> {
    let mut unpadded_str = vec![];
    for i in KEY {
        unpadded_str.push(i.clone());
    }
        
    pad(24, &mut unpadded_str)?;
    println!("{:?}", unpadded_str);

    Ok(())
}

fn pad(len: usize, unpadded_str: &mut Vec<u8>) -> Result<(), &'static str> {
    match unpadded_str.len() < len {
        true => {
            let i = len - unpadded_str.len();
    
            for _ in 0..i {
                unpadded_str.push(4); 
            }
        },
        false => return Err("padding length cannot be less than total length.")
    }

    Ok(())
}
