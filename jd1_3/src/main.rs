extern crate crypto_tools;
extern crate hex;

use crypto_tools::simple_crypt::*;
use crypto_tools::util::*;

fn main() {
    let encoded_str = 
        hex::decode("1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"
        .to_string()
        .into_bytes())
        .unwrap(); 
    let vv = decode_sbxor_ascii(encoded_str);  
    let mut max_score = -1.0;
    let mut soln: String = String::new();
    for v in vv {
        let res: (bool, f32) = is_english(&v);
        if res.0 && res.1 > max_score {
            max_score = res.1;
            soln = v.into_iter().collect();
        }
    }
    println!("{:?}", soln);
}
