extern crate crypto_tools;
extern crate hex;

use crypto_tools::simple_crypt::*;
use crypto_tools::util::is_english;
use std::io::{BufReader,BufRead};
use std::fs::File;

fn main() {
    let enc_strs = pop_enc_matrix();
    let mut soln: String = String::new();
    let mut max_score = -1.0;
    for enc_str in enc_strs {
        let enc_str = hex::decode(enc_str.to_string().into_bytes()).unwrap();
        let dec_vv: Vec<Vec<char>> = decode_sbxor_ascii(enc_str);
        for v in dec_vv {
            let res: (bool, f32) = is_english(&v);
            if res.0 && res.1 > max_score {
                max_score = res.1;
                soln = v.into_iter().collect();
            }
        }
    }
    println!("{:?}", soln);
}

fn pop_enc_matrix() -> Vec<String> {
    let mut encoded_strings = vec![]; 
    let file = File::open("strings.txt").expect("../strings.txt DNE");
    for line in BufReader::new(file).lines() {
        encoded_strings.push(line.unwrap());
    }
    
    encoded_strings
}
