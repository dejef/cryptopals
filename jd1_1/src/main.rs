// Cryptopals hex to base64
// convert hex to base64 the easy way
//
// 49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d  string
// SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t  da hash

extern crate base64;
extern crate hex;
extern crate crypto_tools;

use std::char;
use crypto_tools::util::*;

fn main() {
    let a = vec_u8_to_char(hex::decode("49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d").unwrap());
    let mut s: String = a.into_iter().collect();
    s = base64::encode(&s);
    println!("{}",s);
}
