extern crate crypto_tools;
extern crate hex;

use crypto_tools::simple_crypt::*;
use hex::encode;
use std::str::from_utf8;

fn main() {
    let mut input_phrase = "Burning 'em, if you ain't quick and nimble I go crazy when I hear a cymbal".to_string().into_bytes();
    let encoding_phrase = "ICE".to_string().into_bytes();
    rkxor_ascii_key(&mut input_phrase, encoding_phrase);
    println!("{:?}", encode(from_utf8(&input_phrase).unwrap()));
}
