extern crate bit_vec;
extern crate base64;
extern crate crypto_tools;
extern crate hex;

use crypto_tools::util::*;
use crypto_tools::simple_crypt::*;
use base64::{decode_config};


fn main() {
    let text: Vec<u8> = vec_char_to_u8(file_to_vec("6.txt".to_string())); 
    let text = decode_config(&text, base64::MIME).unwrap(); 
    let text = match String::from_utf8(text) {
        Ok(v) => v,
        Err(e) => panic!("Invalid UTF-8 sequence: {}", e),
    };

    let candidate_keysizes = find_keysizes(text.as_str());   // returns vec of (distance, keysize)
   
    if !candidate_keysizes.is_empty() {
        let mut min = candidate_keysizes[0].0;  // first key is only known min
        let mut keysize = 0;                    // initial keysize when none are known
        for key in candidate_keysizes {        
            if min > key.0 {
                min = key.0;
                keysize = key.1; 
            }
        }
        let split_blocks = split_blocks_keysize(&text, keysize);
        let transposed_blocks = transpose(split_blocks, keysize);
        let mut all_enc_bytes = vec![]; 
        for transposed_block in transposed_blocks {
            let dec_vv = decode_sbxor_ascii(transposed_block.into_bytes());
            let mut max_score = -1.0;
            let mut enc_byte = 0;
            for i in 0..255 {
                let res: (bool, f32) = is_english(&dec_vv[i]);    
                if res.0 && res.1 > max_score {
                    max_score = res.1;
                    enc_byte = i as u8; 
                }
            }
            all_enc_bytes.push(enc_byte);
        }
        let fin: String = vec_u8_to_char(all_enc_bytes).into_iter().collect();
        println!("{:?}", fin);
    }
}
