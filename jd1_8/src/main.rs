extern crate base64;
extern crate crypto_tools;

use crypto_tools::util::*;

static FILENAME: &'static str = "8.txt";

fn main() -> std::io::Result<()> {
    // This attack probably only works on plaintext that has a repetition.
    let mut hex_chars = file_to_vec(FILENAME.to_string()); 
   
    // this is an O(n) operation that could be avoided with more custom code
    hex_chars.retain(|&x| x != '\n');

    // every 320 characters may have some repetition in it.
    // there are 204 hex strings of len 320.
    let mut char_count = 0;
    let mut seq_count = 0;
    let init_chr: char = '0';
    let mut seq_list: Vec<Vec<char>> = vec![vec![init_chr;320];204]; 
    for hex_char in hex_chars {
         if char_count == 320 {
            // new sequence
            seq_count += 1;
            char_count = 0;
         } 
         seq_list[seq_count][char_count] = hex_char;
         char_count += 1;
    }
    
    let mut index = 0;
    let mut maybe_match = 0;
    let mut max_dups = 0;
    for seq in seq_list { // seq has len of 204
        let mut chr_count = 0;
        let mut seq_count = 0;
        let mut seq_strs: Vec<String> = vec![String::new();20];
        for chr in seq {
            if chr_count == 16 {
                seq_count += 1;
                chr_count = 0;
            }
            seq_strs[seq_count].push(chr); 
            chr_count += 1;
        }
        for i in 0..seq_strs.len() {
            let mut dups = 0;
            for j in 0..seq_strs.len() {
                if seq_strs[i] == seq_strs[j] && i != j {
                    dups += 1;
                }
            }
            if max_dups < dups {
                max_dups = dups;
                maybe_match = index;
            }
        }
        index += 1;
    }
    println!("# of duplicates: {}\nString #: {}", max_dups, maybe_match);

    Ok(()) 
}
