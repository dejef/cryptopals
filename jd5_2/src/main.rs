// first thing is to implement diffie hellman 
extern crate base64;
extern crate num_bigint;
extern crate num_traits;
extern crate rand;
extern crate crypto;

use std::io::prelude::*;
use std::fs::File;
use num_bigint::{BigUint, RandomBits};
use num_traits::{Zero, One};
use rand::Rng;
use crypto::sha1::Sha1;
use crypto::digest::Digest;
use crypto::aes::{self, KeySize};
use crypto::buffer::{self, RefReadBuffer, RefWriteBuffer, ReadBuffer, WriteBuffer};
use crypto::symmetriccipher::SynchronousStreamCipher;
use crypto::blockmodes::{self};

struct Entity {
    msg: String,
    rand: BigUint,
    pub_key: BigUint
}

fn main() {
    let mut rng = rand::thread_rng();
    let mut alice_msg: String = "Hello, World!".to_string();
    let mut bob_msg: String = "Welcome to Wonderland!".to_string();

    //create person A    
    let a: BigUint = rng.sample(RandomBits::new(32));

    // Make p into a BigUint 
    let b: BigUint = rng.sample(RandomBits::new(32));
    let g: Vec<u32> = vec![2;1]; // BigUint represents nums in radix 2^32
    let g: BigUint = BigUint::new(g);
    let p: BigUint = BigUint::from_bytes_le(
            "ffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc740\
             20bbea63b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374f\
             e1356d6d51c245e485b576625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee3\
             86bfb5a899fa5ae9f24117c4b1fe649286651ece45b3dc2007cb8a163bf0598da\
             48361c55d39a69163fa8fd24cf5f83655d23dca3ad961c62f356208552bb9ed52\
             9077096966d670c354e4abc9804f1746c08ca237327ffffffffffffffff"
             .as_bytes());
    
    let A = g.modpow(&a, &p);
    let B = g.modpow(&b, &p);

    let mut alice = Entity { msg: alice_msg, rand: a, pub_key: A };
    let mut bob = Entity { msg: bob_msg, rand: b, pub_key: B };

    // Haven't simulated the sending of the messages. All working in same scope
    let s1 = alice.pub_key.modpow(&bob.rand, &p).to_str_radix(10); // s1 = (A**b)%p
    let s2 = bob.pub_key.modpow(&alice.rand, &p); // s2 = (A**b)%p

    let mut hasher = Sha1::new();
//    hasher.input_str(&s1.as_str()[..16]);
    hasher.input_str(s1.as_str());
    
    let b64_hex_key = &base64::encode(&hasher.result_str())[..16];

    println!("key: {:?}", b64_hex_key);
    let mut iv: Vec<u8> = vec![0;16];
    for i in iv.iter_mut() {  // note, I did not know iter_mut was a thing.
        *i = rng.gen();
    }

    // create an object that has trait methods for encrypting the contents of a
    // refReadBuffer and outputting them to a refWriteBuffer.
    // Pkcs stands for Public Key Cryptography Standard
    let mut encryptor = aes::cbc_encryptor(
        aes::KeySize::KeySize256,
        b64_hex_key.as_bytes(),
        iv.as_slice(),
        blockmodes::PkcsPadding);

    let iv = base64::encode(iv.as_slice());
    println!("{:?}", iv);

    // number of bytes read into encryptor
    let bytes_read: u64 = 0;

    // create the RefReadBuffer for use by the enryptor
    let mut read_buffer = buffer::RefReadBuffer::new(&b64_hex_key.as_bytes());

    // space to store the output of the encrypted msg
    let mut buffer = [0; 4096];

    // create the RefWriteBuffer to wrap the output of the encryptor
    let mut write_buffer = buffer::RefWriteBuffer::new(&mut buffer);

    // needs an output file to place write_buffer contents
    let mut buffer = File::create("foo.txt").expect("file creation");

    // .encrypt returns a bufferoverflow or a bufferunderflow 
    let result = encryptor.encrypt(&mut read_buffer, &mut write_buffer, bytes_read == alice.msg.len() as u64)
                 .expect("encrypt data");

    // learn how to print a write buffer for debugging

    buffer.write_all(write_buffer.take_read_buffer().take_remaining()).expect("write file");
}

// https://github.com/DaGenix/rust-crypto/issues/330
// http://siciarz.net/24-days-of-rust-rust-crypto/
// https://stackoverflow.com/questions/51640381/how-do-i-generate-a-random-numbiguint/51641222
